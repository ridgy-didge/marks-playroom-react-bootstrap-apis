import Home from '../../views/home';
import fOne from '../../views/f1';
import Blender from '../../views/blender';
import Error404 from '../../views/error-404';

const Menuitems = [
  { id: 1, path: '/', component: Home, match: 'exact' },
  { id: 2, path: '/f1', component: fOne },
  { id: 3, path: '/blender', component: Blender },
  { id: 4, path: '', component: Error404 },
];
export default Menuitems;