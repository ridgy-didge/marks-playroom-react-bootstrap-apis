import React, { Component } from 'react';
import { BrowserRouter } from 'react-router-dom';
//bootstrap

//styles
import './App.scss';
// Components
import NavRouter from './components/router'

class App extends Component {
  render() {
    return (
      <section className="app">
        <BrowserRouter>
          <NavRouter/>
        </BrowserRouter>
      </section>
    )
  }
}

export default App;
