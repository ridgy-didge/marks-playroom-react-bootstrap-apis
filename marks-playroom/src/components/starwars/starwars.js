import React, { Component } from 'react';

//bootstrap
import { Container, Button  } from 'react-bootstrap';
//styles
import './starwars.scss';

class Starwars extends Component {
  render() {
    return (
      <div className="starwars">
        <Container > 
          <section>      
            <h1 >Star Wars</h1>
            <div>
              <Button href="#" alt="test button">Test Button</Button>
            </div>
          </section>   
        </Container>
      </div>
    )
  }
}

export default Starwars;