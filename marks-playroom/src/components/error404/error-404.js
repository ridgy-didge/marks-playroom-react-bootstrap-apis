import React, { Component } from 'react';
//bootstrap
import { Button, Container } from 'react-bootstrap';
//styles
import './error-404.scss';

class Blender extends Component {
  render() {
    return (
      <div className="error-404">
      <Container > 
        <section>      
          <h1 >Page not found</h1>
          <div>
            <Button href="#" alt="test button">Test Button</Button>
          </div>
        </section>   
      </Container>
    </div>
    )
  }
}

export default Blender;