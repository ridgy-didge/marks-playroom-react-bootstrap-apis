import React, { Component } from 'react';

import axios from 'axios';

//bootstrap
import { Container } from 'react-bootstrap';

//styles
import './f1.scss';


const baseUrl = "https://ergast.com/api/f1";

class fOne extends Component {
  constructor() {
    super();
    this.state = {
      error: null,
      isLoaded: false,

      driverID: "",
      driverRaces: [],
      driverName: "",
      lightOn: false,
      totalLights: 0,
      
       // set inital positions
      driver: "senna",
      dialValue: "1",
      dialPosition: 15,

      // digital values for display - position
      dialDisplayValue: "888",
      dialDisplayValue1: "",
      dialDisplayValue2: "",
      dialDisplayValue3: "",

      // digital values for display - results
      racesDisplayValue: "88",
      racesDisplayValue1: "",
      racesDisplayValue2: "",

      opacity:"1",
    }
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleDialSubmit = this.handleDialSubmit.bind(this);
  }

  componentDidMount() {
    this.fetchData(); // get the ball rolling
  }

  fetchData() {  // fetch data
    axios
    .get(baseUrl + "/drivers/" + this.state.driver + "/results/" + this.state.dialValue + ".json?limit=1000")
    .then(
      (data) => {
        this.setState({
            isLoaded: true,
            driverRaces: data.data.MRData.RaceTable.Races,
            driverName: data.data.MRData.RaceTable.driverId,
          }, 
          this.fetchDriver(),
        )
      }
    )
    .catch(error => console.log(error))
  }

  fetchDriver() {  // submit search driver position.
    axios
    .get(baseUrl + "/drivers/" + this.state.driver + ".json?limit=1000")
    .then(
      (data) => {
        this.setState({
            isLoaded: true,
            driverID: data.data.MRData.DriverTable.driverId.substring( 0, 3 ), // cut driver ID to 3 characters
          },
          this.addPositionText(),
          this.addZeros(),
          this.addResultText(),
          this.addPositionText(),     
        );
      }
    )
    .catch(error => console.log(error))
  }

  handleChange(event) { // input driver name changes set to state.
    event.preventDefault();
    this.setState({
      driver: event.target.value,
    });
  }

  handleSubmit(event) {  // submit search driver.
    event.preventDefault(); 
    this.setState({
        isLoaded: false,
      },
      () => {  // wait until the state has updated before fetching data
        this.fetchData()
      }
    )
  }

  handleDialSubmit(event) {  // submit driver position using the dial.
    event.preventDefault(); 
    this.setState({ // set dial value and position
        isLoaded: false,
        dialValue: event.target.dataset.value,
        dialPosition: 15 * event.target.dataset.value,
      },  
      () => { // wait until the state has updated before fetching data
        this.fetchData()
      }
    )
  }


  addZeros() {
    if (this.state.dialValue <= 9 ) {
      this.setState({
       dialDisplayValue: "00" + this.state.dialValue
      })
    }
    if (this.state.dialValue >= 10 && this.state.dialValue <=99 ) {
      this.setState({
       dialDisplayValue: "0" + this.state.dialValue
      })
    } 
    if (this.state.dialValue >= 100 ) {
      this.setState({
        dialDisplayValue: this.state.dialValue
      })
    }

    // count the race finishes, convert to string and add 0 if required
    if (this.state.driverRaces.length <= 9 ) {
      this.setState({
       racesDisplayValue: "0" + this.state.driverRaces.length.toString()
      })
    } else {
      this.setState({
        racesDisplayValue: this.state.driverRaces.length.toString()
      })
    } 
  }

  addPositionText() {
    if (this.state.dialDisplayValue.substring(0,1) === "1" ) {
      this.setState({
        dialDisplayValue1: <text className="cls-4" transform="translate(358 197)">{this.state.dialDisplayValue.substring(0,1)}</text> 
      })
    } else {
      this.setState({
        dialDisplayValue1: <text className="cls-4" transform="translate(338 197)">{this.state.dialDisplayValue.substring(0,1)}</text> 
      })
    }
    if (this.state.dialDisplayValue.substring(1,2) === "1" ) {
      this.setState({
        dialDisplayValue2:<text className="cls-4" transform="translate(408 197)">{this.state.dialDisplayValue.substring(1,2)}</text>
      })
    } else {
      this.setState({
        dialDisplayValue2: <text className="cls-4" transform="translate(383 197)">{this.state.dialDisplayValue.substring(1,2)}</text>
      })
    }
    if (this.state.dialDisplayValue.substring(2,3) === "1" ) {
      this.setState({
        dialDisplayValue3: <text className="cls-4" transform="translate(454 197)">{this.state.dialDisplayValue.substring(2,3)}</text>
      })
    } else {
      this.setState({
        dialDisplayValue3: <text className="cls-4" transform="translate(429 197)">{this.state.dialDisplayValue.substring(2,3)}</text>
      })
    }
  }

  addResultText() {
    if (this.state.racesDisplayValue.substring(0,1) === "1" ) {
      this.setState({
        racesDisplayValue1: <text className="cls-9" transform="translate(225 208)">{this.state.racesDisplayValue.substring(0,1)}</text>
      })
    } else {
      this.setState({
        racesDisplayValue1: <text className="cls-9" transform="translate(191 208)">{this.state.racesDisplayValue.substring(0,1)}</text>
      })
    }
    if (this.state.racesDisplayValue.substring(1,2) === "1" ) {
      this.setState({
        racesDisplayValue2: <text className="cls-9" transform="translate(287 208)">{this.state.racesDisplayValue.substring(1,2)}</text>
      })
    } else {
      this.setState({
        racesDisplayValue2: <text className="cls-9" transform="translate(252 208)">{this.state.racesDisplayValue.substring(1,2)}</text>
      })
    }    
  }

  render() {
    const lights = this.state.driverRaces.length / 2 ;
    console.log(lights);

    if ( lights >= 0 || lights <= 1 ){
      var light1 =  <circle id="greenlight-1" data-name="light-1" className="cls-24" cx="43.77" cy="70.94" r="12.5"/>;
    }
    if ( lights >= 2 ){
      var light2 =   <circle id="greenlight-2" data-name="light-2" className="cls-24" cx="76.02" cy="69.15" r="12.5"/>
    }
    if ( lights >= 3 ){
      var light3 =  <circle id="greenlight-3" data-name="light-3" className="cls-24" cx="108.26" cy="67.36" r="12.5"/>
    }
    if ( lights >= 4 ){
      var light4 =  <circle id="greenlight-4" data-name="light-4" className="cls-24" cx="140.51" cy="65.57" r="12.5"/>
    }
    if ( lights >= 5 ){
      var light5 =  <circle id="greenlight-5" data-name="light-5" className="cls-24" cx="172.76" cy="63.78" r="12.5"/>
    }
    if ( lights >= 6 ){
      var light6 =  <circle id="greenlight-6" data-name="light-6" className="cls-24" cx="205.01" cy="61.99" r="12.5"/>
    }
    if ( lights >= 7 ){
      var light7 =   <circle id="greenlight-7" data-name="light-7" className="cls-24" cx="237.26" cy="60.2" r="12.5"/>
    }
    if ( lights >= 8 ){
      var light8 =   <circle id="amberlight-1" data-name="light-8" className="cls-26" cx="269.51" cy="60.18" r="12.5"/>
    }
    if ( lights >= 9 ){
      var light9 =   <circle id="amberlight-2" data-name="light-9" className="cls-26" cx="301.75" cy="61.98" r="12.5"/>
    }
    if ( lights >= 10 ){
      var light10 =   <circle id="amberlight-3" data-name="light-10" className="cls-26" cx="334" cy="63.77" r="12.5"/>
    }
    if ( lights >= 11 ){
      var light11 =   <circle id="amberlight-4" data-name="light-11" className="cls-26" cx="366.25" cy="65.56" r="12.5"/>
    }
    if ( lights >= 12 ){
      var light12 =   <circle id="redlight-1" data-name="light-12" className="cls-18 " cx="398.5" cy="67.36" r="12.5"/>
    }
    if ( lights >= 13 ){
      var light13 =  <circle id="redlight-2" data-name="light-13" className="cls-18" cx="430.75" cy="69.15" r="12.5"/>
    }
    if ( lights >= 14 ){
      var light14 =   <circle id="redlight-3" data-name="light-14" className="cls-18" cx="463" cy="70.94" r="12.5"/> 
    }



    return (
      <div className="formula-one">
        <Container className="formula-one-container"> 
            <h1 className="area-title">Formula One Driver Career Results</h1>
            <p>Data from Ergast f1 API</p>
            <form className="form" onSubmit={this.handleSubmit}> 
              <label className="input-container">
                <input type="text" value={this.state.driver} onChange={this.handleChange} className="input-box"/>
              </label>
              <input type="submit" value="Submit" />
            </form> 
            <svg id="careerDial" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 500 500">
              <title>Career Dial</title>

              <radialGradient id="rad-grad-1" cx="250" cy="360" r="135" gradientUnits="userSpaceOnUse">
                <stop  offset="0%" stopColor="rgba(0,0,0,0.5)"/>
                <stop  offset="90%" stopColor="rgba(255,255,255,0.5)" />
              </radialGradient>

              <linearGradient id="glass-gradient" x2="1" y2="1">
                <stop offset="0%" stopColor="#ffffff" />
                <stop offset="50%" stopColor="transparent" />

                <stop offset="100%" stopColor="transparent" />
              </linearGradient>
              <linearGradient id="lcd-bg" x2="1" y2="1">
                <stop offset="0%" stopColor="blue" />
                <stop offset="50%" stopColor="none" />
                <stop offset="100%" stopColor="blue" />
              </linearGradient>

              <rect id="display-bg-2" data-name="display-bg" className="cls-2" x="31" y="124.92" width="143" height="84.5"/>
              <text className="cls-3" transform="translate(32 197)">888</text> 
              <text className="cls-4" transform="translate(32 197)">{this.state.driverID}</text>
              <rect id="display-front" className="cls-5" x="31" y="124.92" width="143" height="84.5"/>
              <rect id="display-bg-2" data-name="display-bg" className="cls-2" x="333" y="124.92" width="143" height="84.5"/>
              <text className="cls-6" transform="translate(338.17 197.18)">888</text> 

              {this.state.dialDisplayValue1}
              {this.state.dialDisplayValue2}
              {this.state.dialDisplayValue3}
              
              <rect id="display-front-2" data-name="display-front" className="cls-5" x="332" y="124.92" width="143" height="84.5"/>
              <rect id="display-bg-3" data-name="display-bg" className="cls-7" x="190.5" y="114.67" width="124" height="105"/>
              <text className="cls-8" transform="translate(190.57 207.88)">88</text> 

              {this.state.racesDisplayValue1}
              {this.state.racesDisplayValue2}

              <rect id="display-front-3" data-name="display-front" className="cls-5" x="190" y="113.1" width="124" height="106"/>
              <circle className="cls-10" cx="253" cy="362.17" r="135"/>
              <circle className="cls-11" cx="253" cy="362.17" r="125.67"/>
              <circle className="cls-12" cx="253" cy="362.17" r="97.5"/>
              <g>
                <path type="button" data-value="22" onClick={this.handleDialSubmit} className="cls-13" d="M215.79,299.34l-25-42.13A122.73,122.73,0,0,0,166.62,276l34.7,34.6A73.68,73.68,0,0,1,215.79,299.34Z"/>
                <path type="button" data-value="23" onClick={this.handleDialSubmit} className="cls-13" d="M233.27,291.86l-13.2-47.19a121.56,121.56,0,0,0-28.24,11.92L216.42,299A72.55,72.55,0,0,1,233.27,291.86Z"/>
                <path type="button" data-value="19" onClick={this.handleDialSubmit} className="cls-13" d="M180,362.27v-.1a73.26,73.26,0,0,1,2.26-18.12l-47.48-12.12A122.41,122.41,0,0,0,131,362.17v.1h49Z"/>
                <path type="button" data-value="24" onClick={this.handleDialSubmit} className="cls-13" d="M252.14,289.17l-.56-49a122.82,122.82,0,0,0-30.33,4.17L234,291.66A73.29,73.29,0,0,1,252.14,289.17Z"/>
                <path type="button" data-value="20" onClick={this.handleDialSubmit} className="cls-13" d="M189.35,326.4l-42.69-24.06a121.76,121.76,0,0,0-11.57,28.4l47.36,12.6A72.13,72.13,0,0,1,189.35,326.4Z"/>
                <path type="button" data-value="21" onClick={this.handleDialSubmit} className="cls-13" d="M200.8,311.14l-35-34.26a122.55,122.55,0,0,0-18.49,24.39l42.44,24.49A73,73,0,0,1,200.8,311.14Z"/>
                <path type="button" data-value="4" onClick={this.handleDialSubmit} className="cls-13" d="M315.8,324.92l42.12-25a122.42,122.42,0,0,0-18.81-24.14l-34.59,34.71A73.35,73.35,0,0,1,315.8,324.92Z"/>
                <path type="button" data-value="1" onClick={this.handleDialSubmit} className="cls-13" d="M271.08,291.42l12.09-47.49A122.25,122.25,0,0,0,253,240.17h-.16v49H253A73,73,0,0,1,271.08,291.42Z"/>
                <path type="button" data-value="6" onClick={this.handleDialSubmit} className="cls-13" d="M326,361.27l49-.59a122.28,122.28,0,0,0-4.18-30.32l-47.32,12.75A72.77,72.77,0,0,1,326,361.27Z"/>
                <path type="button" data-value="5" onClick={this.handleDialSubmit} className="cls-13" d="M323.3,342.4l47.18-13.23a121.8,121.8,0,0,0-11.94-28.24l-42.37,24.63A72.35,72.35,0,0,1,323.3,342.4Z"/>
                <path type="button" data-value="2" onClick={this.handleDialSubmit} className="cls-13" d="M288.74,298.49l24-42.7a121.6,121.6,0,0,0-28.41-11.55L271.79,291.6A72.78,72.78,0,0,1,288.74,298.49Z"/>
                <path type="button" data-value="3" onClick={this.handleDialSubmit} className="cls-13" d="M304,309.94l34.24-35.06a123,123,0,0,0-24.4-18.48l-24.46,42.46A73.51,73.51,0,0,1,304,309.94Z"/>
                <path type="button" data-value="15" onClick={this.handleDialSubmit} className="cls-13" d="M201.94,414.34l-34.27,35A122.52,122.52,0,0,0,192,467.87l24.52-42.43A73.7,73.7,0,0,1,201.94,414.34Z"/>
                <path type="button" data-value="10" onClick={this.handleDialSubmit} className="cls-13" d="M290.14,425l25,42.16a123,123,0,0,0,24.16-18.77l-34.65-34.64A73.71,73.71,0,0,1,290.14,425Z"/>
                <path type="button" data-value="11" onClick={this.handleDialSubmit} className="cls-13" d="M272.65,432.5l13.15,47.2a121.21,121.21,0,0,0,28.26-11.9l-24.55-42.4A72.41,72.41,0,0,1,272.65,432.5Z"/>
                <path type="button" data-value="9" onClick={this.handleDialSubmit} className="cls-13" d="M305.15,413.25l35,34.3a122.57,122.57,0,0,0,18.52-24.37l-42.42-24.53A73.57,73.57,0,0,1,305.15,413.25Z"/>
                <path type="button" data-value="8" onClick={this.handleDialSubmit} className="cls-13" d="M316.62,398l42.66,24.1a121.32,121.32,0,0,0,11.59-28.39l-47.34-12.65A72.34,72.34,0,0,1,316.62,398Z"/>
                <path type="button" data-value="7" onClick={this.handleDialSubmit} className="cls-13" d="M375,362H326v.21a73.42,73.42,0,0,1-2.28,18.19l47.47,12.17A122.44,122.44,0,0,0,375,362.17Z"/>
                <path type="button" data-value="12" onClick={this.handleDialSubmit} className="cls-13" d="M253.78,435.16l.51,49A122.39,122.39,0,0,0,284.62,480l-12.68-47.33A72.75,72.75,0,0,1,253.78,435.16Z"/>
                <path type="button" data-value="13" onClick={this.handleDialSubmit} className="cls-13" d="M234.84,432.9,222.7,480.37a122.43,122.43,0,0,0,30.3,3.8h.05l-.05-49A73.64,73.64,0,0,1,234.84,432.9Z"/>
                <path type="button" data-value="17" onClick={this.handleDialSubmit} className="cls-13" d="M182.68,381.86,135.49,395a121.75,121.75,0,0,0,11.9,28.25l42.4-24.58A72.12,72.12,0,0,1,182.68,381.86Z"/>
                <path type="button" data-value="18" onClick={this.handleDialSubmit} className="cls-13" d="M180,363l-49,.54a122.33,122.33,0,0,0,4.15,30.33l47.33-12.7A73.37,73.37,0,0,1,180,363Z"/>
                <path type="button" data-value="14" onClick={this.handleDialSubmit} className="cls-13" d="M217.19,425.8l-24.08,42.68a122,122,0,0,0,28.4,11.58l12.62-47.35A72.51,72.51,0,0,1,217.19,425.8Z"/>
                <path type="button" data-value="16" onClick={this.handleDialSubmit} className="cls-13" d="M190.16,399.34l-42.15,25A122.74,122.74,0,0,0,166.8,448.5l34.62-34.68A73,73,0,0,1,190.16,399.34Z"/>
              </g>
              <circle className="cls-15" cx="253" cy="362.17" r="70"/>
              <g id="arrow" style={{transform: 'rotate(' + this.state.dialPosition + 'deg)', transition: 'transform 600ms ease-out'}}>
                <g className="cls-16">
                  <path className="cls-17" d="M276.53,432.07l-32.37,3.72a2.61,2.61,0,0,1-2.91-2.59l.08-141.87a2.62,2.62,0,0,1,2.31-2.59h0a2.61,2.61,0,0,1,2.84,2l32.29,138.14A2.61,2.61,0,0,1,276.53,432.07Z"/>
                  <g>
                    <path className="cls-18" d="M244.8,433.45a2,2,0,0,1-2-1.95l.08-133.29a1.94,1.94,0,0,1,1.72-1.93l.23,0a1.93,1.93,0,0,1,1.89,1.51l30.34,129.78a1.94,1.94,0,0,1-1.67,2.38L245,433.43Z"/>
                    <path className="cls-19" d="M244.88,296.76a1.45,1.45,0,0,1,1.41,1.12l30.33,129.79a1.4,1.4,0,0,1-.22,1.16,1.46,1.46,0,0,1-1,.61L245,432.94h-.17a1.4,1.4,0,0,1-1-.43,1.41,1.41,0,0,1-.43-1l.08-133.29a1.46,1.46,0,0,1,1.28-1.44h.17m0-1-.28,0a2.45,2.45,0,0,0-2.17,2.43l-.08,133.28A2.46,2.46,0,0,0,244.8,434l.28,0,30.41-3.5a2.45,2.45,0,0,0,2.11-3L247.26,297.66a2.45,2.45,0,0,0-2.38-1.9Z"/>
                  </g>
                </g>
              </g>
              <g id="digit-dividers-left">
                <line className="cls-20" x1="76" y1="125.5" x2="76" y2="207.83"/>
                <line className="cls-20" x1="122" y1="125.5" x2="122" y2="207.83"/>
              </g>
              <g id="digit-dividers-right">
                <line className="cls-20" x1="383" y1="125.5" x2="383" y2="207.83"/>
                <line className="cls-20" x1="427" y1="125.5" x2="427" y2="207.83"/>
              </g>
              
              <text className="cls-22" transform="translate(332 120.17)">Position</text>
              <text className="cls-22" transform="translate(30 120.17)">name</text>
              <text className="cls-22" transform="translate(190 105.17)">total</text>
              
              <g id="lights">
              <circle id="greenlight-1" data-name="light-1" className="cls-24" cx="43.77" cy="70.94" r="12.5"/>;
<circle id="greenlight-2" data-name="light-2" className="cls-24 light-off" cx="76.02" cy="69.15" r="12.5"/>
<circle id="greenlight-3" data-name="light-3" className="cls-24 light-off" cx="108.26" cy="67.36" r="12.5"/>
<circle id="greenlight-4" data-name="light-4" className="cls-24 light-off" cx="140.51" cy="65.57" r="12.5"/>
<circle id="greenlight-5" data-name="light-5" className="cls-24 light-off" cx="172.76" cy="63.78" r="12.5"/>
<circle id="greenlight-6" data-name="light-6" className="cls-24 light-off" cx="205.01" cy="61.99" r="12.5"/>
<circle id="greenlight-7" data-name="light-7" className="cls-24 light-off" cx="237.26" cy="60.2" r="12.5"/>
<circle id="amberlight-1" data-name="light-8" className="cls-26 light-off" cx="269.51" cy="60.18" r="12.5"/>
<circle id="amberlight-2" data-name="light-9" className="cls-26 light-off" cx="301.75" cy="61.98" r="12.5"/>
<circle id="amberlight-3" data-name="light-10" className="cls-26 light-off" cx="334" cy="63.77" r="12.5"/>
<circle id="amberlight-4" data-name="light-11" className="cls-26 light-off" cx="366.25" cy="65.56" r="12.5"/>
<circle id="redlight-1" data-name="light-12" className="cls-18 light-off" cx="398.5" cy="67.36" r="12.5"/>
<circle id="redlight-2" data-name="light-13" className="cls-18 light-off" cx="430.75" cy="69.15" r="12.5"/>
<circle id="redlight-3" data-name="light-14" className="cls-18 light-off" cx="463" cy="70.94" r="12.5"/>
               {light1}{light2}{light3}{light4}{light5}{light6}{light7}{light8}{light9}{light10}{light11}{light12}{light13}{light14}
              </g>

              <text className="cls-27" transform="translate(262.5 262.5)">1</text>
              <text className="cls-27" transform="translate(287.5 269)">2</text>
              <text className="cls-27" transform="translate(312.5 283)">3</text>
              <text className="cls-27" transform="translate(352 351)">6</text>
              <text className="cls-27" transform="translate(352 379.5)">7</text>
              <text className="cls-27" transform="translate(345 406)">8</text>
              <text className="cls-27" transform="translate(333 428.5)">9</text>
              <text className="cls-27" transform="translate(310.5 448.5)">10</text>
              <text className="cls-27" transform="translate(289.5 461.5)">11</text>
              <text className="cls-27" transform="translate(261.5 469.5)">12</text>
              <text className="cls-27" transform="translate(234 468.5)">13</text>
              <text className="cls-27" transform="translate(207.5 462)">14</text>
              <text className="cls-27" transform="translate(185.5 448)">15</text>
              <text className="cls-27" transform="translate(166.5 430)">16</text>
              <text className="cls-27" transform="translate(153 406.5)">17</text>
              <text className="cls-27" transform="translate(146 381.5)">18</text>
              <text className="cls-27" transform="translate(144.5 355.5)">19</text>
              <text className="cls-27" transform="translate(150.5 327.5)">20</text>
              <text className="cls-27" transform="translate(164 303.5)">21</text>
              <text className="cls-27" transform="translate(183.5 283)">22</text>
              <text className="cls-27" transform="translate(206.5 271.5)">23</text>
              <text className="cls-27" transform="translate(333 303)">4</text>
              <text className="cls-27" transform="translate(345.5 326.5)">5</text>
              <text className="cls-27" transform="translate(232.5 264.5)">24</text>
            </svg>             
        </Container>
      </div>
    )
  }
}

export default fOne;