import React, { Component } from 'react';

//styles
import './toggle-nav-01.scss';


class NavToggler extends Component {
  constructor(props, context) {
      super(props, context);
      this.state = {
        open: false,
      };
  }
  render() {
    return (
      <svg className="circle-slide-container" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 500 250">    
        <g id="circle-slide">
          <line id="line" className="cls-1" x1="10" y1="125" x2="490" y2="125"/>
          <circle id="button" className="cls-2" cx="125" cy="125" r="115"/>
          <polygon id="arrow_left" data-name="arrow left" className="cls-3" points="50 109.38 140.2 109.38 103.74 72.92 147.91 72.92 200 125.01 147.91 177.09 103.74 177.09 140.2 140.62 50 140.62 50 109.38"/>
          <polygon id="arrow_right" data-name="arrow right" className="cls-3" points="460 140.63 369.81 140.63 406.27 177.09 362.09 177.09 310 125 362.09 72.92 406.27 72.92 369.81 109.38 460 109.38 460 140.63"/>
        </g>
    </svg>
    )
  }
}
export default NavToggler;