import React, { Component } from 'react';

//bootstrap
import {  Button, Container } from 'react-bootstrap';
//styles
import '../styles/_base.scss';
//images


class Footer extends Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
        };
    }

    render() {
        return (
            <footer className="footer">
                <Container className="footer-social" >
                    <p>
                        <Button className="btn-social" variant="secondary" href="https://www.linkedin.com/in/mark-almond-3909318/">Linkedin</Button>
                    </p>
                    <p>
                        <Button className="btn-social" variant="secondary" href="https://bitbucket.org/ridgy-didge/">Bitbucket</Button>
                    </p>
                </Container>
            </footer>
        );
    }
}

export default Footer;
