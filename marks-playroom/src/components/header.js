import React, { Component } from 'react';

//bootstrap

//styles
import '../styles/_base.scss';
//images

import Nav from './main-nav';

class Header extends Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
        };
    }

    render() {
        return (
            <header className="header-wrapper">
                <Nav />
            </header>
        );
    }
}

export default Header;
