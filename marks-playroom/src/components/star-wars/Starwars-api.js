import React, { Component } from 'react';
import { CSSTransition } from "react-transition-group";

import './styles/Starwars.scss';
import StarwarsApiResults from './Starwars-api-results';
import StarwarsEyeColour from './partials/Starwars-eye-colour';
import StarwarsSkinColour from './partials/Starwars-skin-colour';
import StarwarsHeight from './partials/Starwars-height';
import StarwarsMass from './partials/Starwars-mass';
import StarwarsRandomLetters from './partials/Starwars-random-letters';
import StarwarsClock from './partials/Starwars-clock';
import StarwarsCircleLight from './partials/Starwars-circle-light';

const baseUrl = 'https://swapi.co/api/';

class StarWarsApi extends Component {
  constructor() {
    super();
    this.state = {
      people: [],
      loaded: false,
      value: 'luke'
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    fetch(baseUrl + 'people')
    .then(res => res.json())
    .then(
      (result) => {
        this.setState({
          people: result.results,
          loaded: true
        });
      },
    )
  }

  handleChange(event) {
    this.setState({
      value: event.target.value
    });
  }  

  handleSubmit(event) {
    event.preventDefault();
    this.setState({
      loaded: false
    });
    fetch(baseUrl + 'people/?search=' + this.state.value)
    .then(res => res.json())
    .then(
      (result) => {
        this.setState({
          people: result.results,
          loaded: true
        });
      }
    )
  }

  render() {
    const { people } = this.state;
    console.log(this.state);
    return (
      <div className="d-flex flex-column main-content-holder justify-content-between">
        <div className="d-flex">
          <form className="d-flex search-form flex-column align-items-center justify-content-around flex-lg-row" onSubmit={this.handleSubmit}> 
            <div className="d-flex people-results-container flex-column flex-md-row justify-content-around">
              {people.slice(0, 4).map( (person, index) => (
                <p key={index} className="col">
                  {person.name}
                </p>
              ))} 
            </div>
            <div className="search-container d-flex flex-column justify-self-end">
              <label className="">
                <input type="text" value={this.state.value} onChange={this.handleChange} className="col-12"/>
              </label>
              <input className="submit-button" type="submit" value="Search" />
            </div>
          </form>   
        </div>
        <CSSTransition 
          in={this.state.loaded}
          classNames={'star-wars'}
          timeout={{
            enter:750,
            exit:0
          }}
        >
          <div>
            <div className="col-12 d-lg-flex centre-main-content-holder justify-content-lg-between">
              <div className="d-flex flex-column col-lg-9">
                <div className="d-flex">
                  <StarwarsApiResults value={this.state.value} people={people}/>
                </div>
                <div className="colors-container d-lg-flex">
                  <StarwarsEyeColour people={people} />
                  <StarwarsSkinColour people={people} />
                  <StarwarsMass people={people} />
                </div>
                <div className="d-flex">
                  <StarwarsClock />
                  <div className="d-flex col-1 justify-content-center flex-column flex-sm-row align-content-wrap">
                    <StarwarsRandomLetters />
                    <StarwarsRandomLetters />
                    <StarwarsRandomLetters />
                    <StarwarsRandomLetters />
                    <StarwarsRandomLetters />
                  </div>
                  <div className="d-flex col-1 offset-1 justify-content-center flex-column flex-sm-row">
                    <StarwarsRandomLetters />
                    <StarwarsRandomLetters />
                    <StarwarsRandomLetters />
                  </div>
                </div>
                
              </div>
              <StarwarsHeight people={people} />
            </div>
            <div className="d-flex light-board d-md-flex justify-content-between align-items-stretch align-self-bottom bottom-main-container">
              <div className="col-6 circular-lights-container d-flex justify-content-end">
                <StarwarsCircleLight />
                <StarwarsCircleLight />
                <StarwarsCircleLight />
              </div>
              <div className="col-2">
              </div>
              <div className="col-4 circular-lights-container d-flex justify-content-start">
                <StarwarsCircleLight />
                <StarwarsCircleLight />
              </div>
            </div> 
          </div>
        </CSSTransition>
      </div> 
    );
  }
}

export default StarWarsApi;