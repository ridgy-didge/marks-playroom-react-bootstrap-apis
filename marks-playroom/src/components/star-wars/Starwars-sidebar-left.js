import React, { Component } from 'react';
import './styles/Starwars.scss';

class StarwarsSidebarLeft extends Component {
  constructor() {
    super();
    this.state = {
    }
  }

  componentDidMount() {
   
  }

  render() {
    return (
      <div className="leftbar-container">
          <div className="">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 300 4000" preserveAspectRatio="xMaxYMin meet">
              <polygon className="polygon" points="80,-10 120,-10 120,100 180,200 80,4000" />

              <line className="cls-3" x1="150" y1="0" x2="150" y2="6000" />
              <rect className="cls-4" x="19" y="0" width="20" height="6000" />
              <rect className="cls-4" x="50" y="0" width="20" height="6000" />

              <rect className="cls-4" x="190" y="0" width="20" height="20" />
              <rect className="cls-4" x="220" y="0" width="20" height="20" />
              <rect className="cls-4" x="190" y="40" width="20" height="20" />
              <rect className="cls-4" x="190" y="70" width="20" height="20" />
              
              <rect className="cls-4" x="220" y="110" width="20" height="20" />
              <rect className="cls-4" x="190" y="110" width="20" height="20" />
              <rect className="cls-4" x="220" y="140" width="20" height="20" />

              <rect className="cls-4" x="220" y="180" width="20" height="20" />
              <rect className="cls-4" x="190" y="180" width="20" height="20" />
              <rect className="cls-4" x="190" y="210" width="20" height="20" />

              <rect className="cls-4" x="220" y="250" width="20" height="20" />
            </svg>
          </div>
      </div>
    );
  }
}

export default StarwarsSidebarLeft;