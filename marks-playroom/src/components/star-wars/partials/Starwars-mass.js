import React, { Component } from 'react';
import '../styles/Starwars.scss';

class StarwarsMass extends Component {
  constructor() {
    super();
    this.state = {
    }
  }

  componentDidMount() {
   
  }

  render() {
    let { people } = this.props;
    let mass = people.slice(0, 1).map( person => (
      person.mass
    ))
    let height = 100;
    let centerCircle = height / 2;
    let finalmass = mass / 100 * centerCircle;
    return (
      <div className="justify-content-center mass-results-container">
         <div className="d-flex justify-content-center flex-column">
          <p className="area-title">mass</p>
          <div className="mass-container">
            <svg className="align-self-center"  preserveAspectRatio="xMaxYMin meet">
              <circle cx={centerCircle} cy={centerCircle} r={centerCircle - 2} fill="black" />
              <circle cx={centerCircle} cy={centerCircle} r={finalmass / 2} stroke="" strokeWidth="0" fill="white" />
              <text textAnchor="middle" x="51" y="90" className="svg-text">{mass}</text>
            </svg>
          </div>
        </div>
      </div>
    );
  }
}

export default StarwarsMass;