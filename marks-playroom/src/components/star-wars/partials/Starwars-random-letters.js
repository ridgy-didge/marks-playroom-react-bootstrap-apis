import React, { Component } from 'react';
import '../styles/Starwars.scss';

const letters = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "2", "t"];

class StarwarsRandomLetters extends Component {
  constructor() {
    super();
    this.state = {
      lettersSelected: [],
    }
  }

  componentDidMount() {
    this.setState( {
      lettersSelected: letters[Math.floor(Math.random()*letters.length)]
    })
    
    this.timerID = setInterval(
      () => this.tick(),
      Math.floor(Math.random(2000)*4000),
    )
  }

  componentWillUnmount() {
    clearInterval(this.timerID);
  }

  tick() {
    this.setState( {
      lettersSelected: letters[Math.floor(Math.random()*letters.length)]
    })
  }

  render() {
    // get 5 random letters from array and change at random intervals between 3 and 10 seconds

    return (
      <div className="random-letters-container">
        <p>
          {this.state.lettersSelected}
        </p>
      </div>
    );
  }
}

export default StarwarsRandomLetters;