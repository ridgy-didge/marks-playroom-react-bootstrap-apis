import React, { Component } from 'react';
import '../styles/Starwars.scss';

class StarwarsHeight extends Component {
  constructor() {
    super();
    this.state = {
    }
  }

  componentDidMount() {
   
  }

  render() {
    let { people } = this.props;
    let height = people.slice(0, 1).map( person => (
      person.height
    ))
    let finalscale = height * 2;
    let finalheighttext = finalscale + 10;
    return (
      <div className="col-12 col-lg-3 d-flex height-results-container justify-content-center">
        <div className="height-result justify-content-center d-flex flex-column">
          <svg className="align-self-center" >
            <line x1="0" y1="0" x2="0" y2="600" />
            <line x1="0" y1="0" x2="10" y2="0" />
            <line x1="0" y1="40" x2="10" y2="40" />
            <line x1="0" y1="80" x2="10" y2="80" />
            <line x1="0" y1="120" x2="10" y2="120" />
            <line x1="0" y1="160" x2="10" y2="160" />
            <line x1="0" y1="200" x2="10" y2="200" />
            <line x1="0" y1="240" x2="10" y2="240" />
            <line x1="0" y1="280" x2="10" y2="280" />
            <line x1="0" y1="320" x2="10" y2="320" />
            <line x1="0" y1="360" x2="10" y2="360" />
            <line x1="0" y1="400" x2="10" y2="400" />
            <line x1="0" y1="440" x2="10" y2="440" />
            <line x1="0" y1="480" x2="10" y2="480" />
            <line x1="0" y1="520" x2="10" y2="520" />
            <line x1="0" y1="560" x2="10" y2="560" />
            <line x1="0" y1="600" x2="10" y2="600" />
            <rect x="20" y="0" width="20" height={finalscale} />
            <text x="0" y="-100" className="svg-text-title">Height</text>
            <text x="-100" y={"-" + finalheighttext} className="svg-text">{height}</text>
          </svg>
        </div>
      </div>
    );
  }}

export default StarwarsHeight;