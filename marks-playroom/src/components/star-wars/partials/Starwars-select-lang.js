import React, { Component } from 'react';
import '../styles/Starwars.scss';

class StarwarsLanguageSelect extends Component {
  constructor() {
    super();
    this.state = { 
      language: false,
      elements: []
     };
    this.handleClickHuman = this.handleClickHuman.bind(this);
    this.handleClickAurebash = this.handleClickAurebash.bind(this);
  }

  componentDidMount() {  }

  handleClickHuman(event) {
    event.preventDefault();
    this.setState({language: true});
    this.starwarsfont = React.createRef();
  }  

  handleClickAurebash(event) { 
    event.preventDefault(); 
    this.setState({language: false});
  }

  render() {    
    return (
      <div className="select-lang-container align-content-md-center"> 
        <p className="starwars">Language</p>
        <button className="aurebash col" href="" onClick={this.handleClickAurebash}>
          AUREBASH
        </button><br/>
        <button className="starwars col" href="" onClick={this.handleClickHuman}>
          Human
        </button>
      </div>
    );
  }
}

export default StarwarsLanguageSelect;