import React, { Component } from 'react';
import '../styles/Starwars.scss';

class StarwarsSkinColour extends Component {
  constructor() {
    super();
    this.state = { }
   
  }

  componentDidMount() { }

  colors() {
    
  }
  render() {
    let { people } = this.props;
    let skinColor = people.slice(0, 1).map( person => (
      person.skin_color
    ))

    const style = {
      backgroundColor: skinColor,
    }
    
   
    
    return (
      <div className="col-12 col-lg-4 justify-content-center skin-results-container">
        <div className="d-flex justify-content-center flex-column">
          <p className="area-title">Skin Colour</p>
          <div className="skin-colour-container" style={style}>
        <p>{skinColor}</p> 
        </div>
        </div>
      </div>
    );
  }}

export default StarwarsSkinColour;