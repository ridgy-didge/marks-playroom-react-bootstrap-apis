import React, { Component } from 'react';
import '../styles/Starwars.scss';

class StarwarsEyeColour extends Component {
  constructor() {
    super();
    this.state = {
    };
  }

  componentDidMount() {
  }

  render() {
    let { people } = this.props;
    let eyeColor = people.slice(0, 1).map( person => (
      person.eye_color
    ))

    const style = {
      backgroundColor: eyeColor,
    }

    return (
      <div className="col-12 col-lg-4 justify-content-center eye-results-container">
        <div className="d-flex justify-content-center flex-column">
          <p className="area-title">Eye Colour</p>
          <div className="eye-colour-container" style={style}>
            <p>{eyeColor}</p>
          </div>
        </div>
      </div>
    );
  }
}

export default StarwarsEyeColour;