import React, { Component } from 'react';
import './styles/Starwars.scss';

const feedData = [
    { title: "Luke Skywalker", excerpt: "Nullam a sem quis turpis imperdiet interdum. Donec nisl nisi, tempus in mauris imperdiet, auctor scelerisque dui." },
    { title: "ar Skywalker", excerpt: "Nullam a sem quis turpis imperdiet interdum. Donec nisl nisi, tempus in mauris imperdiet, auctor scelerisque dui." },
    { title: "vcx Skywalker", excerpt: "Nullam a sem quis turpis imperdiet interdum. Donec nisl nisi, tempus in mauris imperdiet, auctor scelerisque dui." },
    { title: ",jg Skywalker", excerpt: "Nullam a sem quis turpis imperdiet interdum. Donec nisl nisi, tempus in mauris imperdiet, auctor scelerisque dui." },
    { title: "Ldvjuke Skywalker", excerpt: "Nullam a sem quis turpis imperdiet interdum. Donec nisl nisi, tempus in mauris imperdiet, auctor scelerisque dui." },
    { title: "hdsa Skywalker", excerpt: "Nullam a sem quis turpis imperdiet interdum. Donec nisl nisi, tempus in mauris imperdiet, auctor scelerisque dui." },
    { title: "hju Skywalker", excerpt: "Nullam a sem quis turpis imperdiet interdum. Donec nisl nisi, tempus in mauris imperdiet, auctor scelerisque dui." },
    { title: "xzxLuke Skywalker", excerpt: "Nullam a sem quis turpis imperdiet interdum. Donec nisl nisi, tempus in mauris imperdiet, auctor scelerisque dui." },
    { title: "Luke Skywalker", excerpt: "Nullam a sem quis turpis imperdiet interdum. Donec nisl nisi, tempus in mauris imperdiet, auctor scelerisque dui." },
    { title: "ar Skywalker", excerpt: "Nullam a sem quis turpis imperdiet interdum. Donec nisl nisi, tempus in mauris imperdiet, auctor scelerisque dui." },
    { title: "vcx Skywalker", excerpt: "Nullam a sem quis turpis imperdiet interdum. Donec nisl nisi, tempus in mauris imperdiet, auctor scelerisque dui." },
    { title: ",jg Skywalker", excerpt: "Nullam a sem quis turpis imperdiet interdum. Donec nisl nisi, tempus in mauris imperdiet, auctor scelerisque dui." },
    { title: "Ldvjuke Skywalker", excerpt: "Nullam a sem quis turpis imperdiet interdum. Donec nisl nisi, tempus in mauris imperdiet, auctor scelerisque dui." },
    { title: "hdsa Skywalker", excerpt: "Nullam a sem quis turpis imperdiet interdum. Donec nisl nisi, tempus in mauris imperdiet, auctor scelerisque dui." },
    { title: "hju Skywalker", excerpt: "Nullam a sem quis turpis imperdiet interdum. Donec nisl nisi, tempus in mauris imperdiet, auctor scelerisque dui." },
    { title: "xzxLuke Skywalker", excerpt: "Nullam a sem quis turpis imperdiet interdum. Donec nisl nisi, tempus in mauris imperdiet, auctor scelerisque dui." },
    { title: "Luke Skywalker", excerpt: "Nullam a sem quis turpis imperdiet interdum. Donec nisl nisi, tempus in mauris imperdiet, auctor scelerisque dui." },
    { title: "ar Skywalker", excerpt: "Nullam a sem quis turpis imperdiet interdum. Donec nisl nisi, tempus in mauris imperdiet, auctor scelerisque dui." },
    { title: "vcx Skywalker", excerpt: "Nullam a sem quis turpis imperdiet interdum. Donec nisl nisi, tempus in mauris imperdiet, auctor scelerisque dui." },
    { title: ",jg Skywalker", excerpt: "Nullam a sem quis turpis imperdiet interdum. Donec nisl nisi, tempus in mauris imperdiet, auctor scelerisque dui." },
    { title: "Ldvjuke Skywalker", excerpt: "Nullam a sem quis turpis imperdiet interdum. Donec nisl nisi, tempus in mauris imperdiet, auctor scelerisque dui." },
    { title: "hdsa Skywalker", excerpt: "Nullam a sem quis turpis imperdiet interdum. Donec nisl nisi, tempus in mauris imperdiet, auctor scelerisque dui." },
    { title: "hju Skywalker", excerpt: "Nullam a sem quis turpis imperdiet interdum. Donec nisl nisi, tempus in mauris imperdiet, auctor scelerisque dui." },
    { title: "xzxLuke Skywalker", excerpt: "Nullam a sem quis turpis imperdiet interdum. Donec nisl nisi, tempus in mauris imperdiet, auctor scelerisque dui." },
];


class StarwarsFeed extends Component {
  constructor() {
    super();
    this.state = {
      feeds: [],
    };
  }

  componentDidMount() {
    this.setState( {
      allfeeds: feedData.concat([Math.floor(Math.random()*feedData.length)])
    })
  }

  render() {


    return (
      <div className="feeds-container"> 
          {feedData.map( (feed, index) => (
            <div key={index}>
              <h3>
                {feed.title}
              </h3>
              <p>
                {feed.excerpt}
              </p>
            </div>
          ))} 
      </div>
    );
  }
}

export default StarwarsFeed;