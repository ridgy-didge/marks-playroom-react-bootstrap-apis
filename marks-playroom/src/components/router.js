import React, { Component } from 'react';
import { CSSTransition, TransitionGroup } from 'react-transition-group';

//bootstrap
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
// styles
import '../App.scss';

import Header from './header';
import Footer from './footer';

// import Menuitems from './data/main-nav-data'

import Home from './home/home';
import fOne from './f1/f1';
import Blender from './blender/blender';
import Starwars from './star-wars/Starwars';
import Wordpress from './wordpress/wordpress';


// Does the user's browser support the HTML5 history API?
// If the user's browser doesn't support the HTML5 history API then we
// will force full page refreshes on each page change.

class NavRouter extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }
  
  render() {
    return (
      <Router >
        <Header/>
        <main>
          <Route
            render={({ location }) => {
              const { pathname } = location;
              return (
                <TransitionGroup className="transition-group">
                  <CSSTransition 
                    key={pathname}
                    classNames={'item'}
                    timeout={{
                      enter:1000,
                      exit:1000
                    }}
                  >
                    <section className="route-section page">
                      <Route
                        location={location}
                        render={() => (
                          <Switch>
                            <Route exact path="/" component={Home} />
                            <Route exact path="/f1" component={fOne} />
                            <Route exact path="/starwars" component={Starwars} />
                            <Route exact path="/blender" compoment={Blender} />
                            <Route path="/wordpress" component={Wordpress} />
                          </Switch>
                        )}
                      />
                    </section>
                  </CSSTransition>
                </TransitionGroup>
              );
            }}
          />
        </main>
        <Footer/>
      </Router>
    )
  }
}

export default NavRouter;