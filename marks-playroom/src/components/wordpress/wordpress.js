import React, { Component } from 'react';
import axios from 'axios';
import { BrowserRouter } from 'react-router-dom';

import WordpressRouter from './components/wp-router'

//bootstrap imports

//styles
import './wordpress.scss';

// animated SVGs

// animated svg buttons

//images

class Wordpress extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: "",
      content: "",
      featured_media: "",
     }
  }

  componentDidMount() {
    const baseUrl = "https://dev-ridgy-didge.co.uk/wp-rest-api-tests/wp-json/wp/v2/pages/272";

    axios
    .get(
      baseUrl 
    )
    .then(res => { // fetch products page and set states
      this.setState({ 
        title: res.data.title.rendered,
        content: res.data.content.rendered,
        featured_media: res.data.featured_media,
      })  
    })
    .catch(error => console.log(error))
  }
  
  render() {
    return (
      <section id="wordpressPage" className="wordpress">
        <BrowserRouter>
          <WordpressRouter/>
        </BrowserRouter>
      </section>
    )
  }
}

export default Wordpress;