import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import '../styles/partials/single-product-details.scss';

class SingleProductDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      productTitle: this.props.location.state.productTitle,
      productID: this.props.location.state.productID, 
      productSlug: this.props.location.state.productSlug,
      productMedia: this.props.location.state.productMedia,
      productDesc: this.props.location.state.productDesc,
      productAcf: this.props.location.state.acfDetails,
      productImg: this.props.location.state.productImgUrl,
    };
  }

  rating(rating) {
    let stars = [];
    for (let i = 0; i < rating; i++) {
      stars.push(  
        <svg key={i} viewBox="0 0 25 23" className="star-rating">
          <polygon points="9.9, 1.1, 3.3, 21.78, 19.8, 8.58, 0, 8.58, 16.5, 21.78" fill="rule:nonzero;"/>
        </svg>
      );
    }
    return <div>{stars}</div>;
  }

  render() {
    console.log(this.state)
    return (
      <div className="wp-product">
        <div className="wordpress-product">
          <div className="wp-product-container container">
            <div className="wp-product-header">
              <h2 className="wp-product-title">Wordpress API - Product details </h2>
            </div>   
            <div className="wp-product-container ">
              <div className="wp-product-main">
                <div className="wp-product-header">
                  <div className="wp-product-image-container">
                    <img src={this.state.productImg} alt={this.state.productTitle}/>
                  </div>
                  <div className="wp-product-header-details">
                    <div className="wp-product-title-holder">
                      <h2>{this.state.productTitle}</h2>
                    </div>
                  </div>
                </div>
              </div>
              <div className="wp-product-description-holder">
                <p dangerouslySetInnerHTML={{__html: this.state.productDesc}}/> 
              </div>
              <div className="wp-product-wrapper">
                { this.state.productAcf.product_details.map((acfDetails, index) =>
                  <div key={index} className="wp-product-container">
                    <p>Rating: {acfDetails.star_rating} stars</p>
                    {this.rating(acfDetails.star_rating)}
                    <div className="wp-product-image-container">
                      { acfDetails.product_images.map((images, index) =>
                        <div key={index} className="wp-product-image">
                          <img src={images.image.url} alt={acfDetails.product_title}/>
                        </div>
                      )}
                    </div>
                    <div className="wp-product-details">
                      <div className="wp-product-sku">
                        <p>SKU: {this.state.productID}-{acfDetails.product_colours[0].colour} </p>
                      </div>

                      <div className="wp-product-colours-holder option-holder">
                        <div className="title-holder">
                          <p>Colour:</p>
                        </div>
                        <div className="colour-options-holder">
                          { acfDetails.product_colours.map((colours, index) =>
                            <div key={index} className="colour-option">
                              <p>{colours.colour}</p>
                            </div>
                          )}
                        </div>
                      </div>
                      <h3>Price: £{acfDetails.price}</h3>
                      <div className="wp-product-link-holder">
                        <Link to="#">
                          Buy {acfDetails.product_title}
                        </Link>
                      </div>
                    </div>
                  </div>
                )}
              </div>     
            </div>     
          </div>
        </div>
      </div>
    );
  }
}

export default SingleProductDetails;

