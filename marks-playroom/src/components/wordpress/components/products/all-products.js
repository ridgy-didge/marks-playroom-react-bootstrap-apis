import React, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';

import '../styles/partials/all-products.scss';


class WPProducts extends Component {
  constructor(props) {
    super(props);
    this.state = {
      products: [],
      productList: [],
    };
  }
  
  componentDidMount() {
    const pages = "products";   // filter product result to post type
    const baseUrl = "https://dev-ridgy-didge.co.uk/wp-rest-api-tests/wp-json/wp/v2/";

    axios
    .get(
      baseUrl + pages
    )
    .then(res => { // fetch products
      this.setState({ 
        products: res.data
      })
      
      this.state.products.map((product) =>  // loop products to find featured_media id
      
        axios
        .get(
          baseUrl + `media/${product.featured_media}`   // fetch featured_media image data 
        )
        .then(res => { // fetch products
          this.setState({ 
            productList: this.state.productList.concat({
              image_url: res.data.source_url,  // fetch image url from results data
              title: product.title.rendered,
              slug: product.slug,
              excerpt: product.excerpt.rendered,
              acfDetails: product.acf,
              content: product.content.rendered,
              id: product.id,
              featured_media: product.featured_media,
            }),  
          })
        })
        .catch(error => console.log(error)),
      )
    })
    .catch(error => console.log(error))
  }

  render() {
    console.log(this.state)
    return (
      <section className="wp-products">
        <div className="">
          <div className="wp-products-container container">
            <div className="wp-products-header ">
              <h2 className="wp-products-title">Wordpress API - custom post type "products" with ACFs</h2>
            </div>    
            <div className="products">     
              { this.state.productList.map((product, index) => // loop all products inside ProductList state 
                <div key={product.id + "-" + index} className="products-container">
                  <div className="products-image-container">
                    <img src={[product.image_url]} alt={product.title} />
                  </div>
                  <div className="products-info">
                    <div className="wp-products-title-holder">
                      <h3 >{product.title}</h3>
                    </div>
                    <div className="products-description-holder">
                      <p className="products-description" dangerouslySetInnerHTML={{__html: product.excerpt}}/> 
                    </div>
                    <div className="products-link-holder">
                      <Link to={{
                          pathname: `/wordpress/product/${product.slug}`,
                          state: {
                            productSlug: product.slug,
                            productID: product.id,
                            productTitle: product.title,
                            productMedia: product.featured_media,
                            productImgUrl: product.image_url,
                            productDesc: product.content,
                            acfDetails: product.acfDetails,
                          }
                        }}>
                        {product.title} Info
                      </Link>
                    </div>
                  </div>
                </div>
                )}
            </div>
          </div>
        </div>
      </section>
    )
  }
}

export default WPProducts;
