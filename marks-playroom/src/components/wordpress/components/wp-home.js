import React, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';

import WPPosts from './posts/wp-posts';

//bootstrap imports
import { Container } from 'react-bootstrap';

//styles
import './styles/wp-home.scss';

// animated SVGs

// animated svg buttons

//images
import spaceship from './images/page-bg-spaceship.png'; 

class WPHome extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: "",
      content: "",
      featured_media: "",
     }
  }

  componentDidMount() {
    const baseUrl = "https://dev-ridgy-didge.co.uk/wp-rest-api-tests/wp-json/wp/v2/pages/272";

    axios
    .get(
      baseUrl 
    )
    .then(res => { // fetch products page and set states
      this.setState({ 
        title: res.data.title.rendered,
        content: res.data.content.rendered,
        featured_media: res.data.featured_media,
      })  
    })
    .catch(error => console.log(error))
  }

  render() {

    return (
      <section id="wordpressPage" className="wordpress">
        <article className="wordpress-section wordpress-products">   
          <Container className="wordpress-container">  
            <div className="wordpress-section-title">
              <h2>{this.state.title}</h2>
            </div>
            <div className="wordpress-products-body">
              <div className="wordpress-products-description">
                <p dangerouslySetInnerHTML={{__html: this.state.content}}/> 
              </div>
              <div className="wordpress-products-image">
                <img src={spaceship} alt="Spaceship Model" />
              </div>
            </div>
            <div className="wordpress-products-footer">
              <Link className="btn-page btn" to={{
                  pathname: `/wordpress/products`,
                }}>
                Spaceship Model Products
              </Link>
            </div>
          </Container>
        </article>  
       
        <article className="wordpress-section wordpress-posts">   
          <Container className="wordpress-container">  
            <WPPosts/>
          </Container> 
        </article>  

        <article className="wordpress-section wordpress-pages">     
          <Container className="wordpress-container">   
            <div className="wordpress-section-title">
              <h2>Wordpress Page</h2>
            </div>
            <Link  className="btn-page btn" to={{
                pathname: `/wp-pages/page`,
              }}>
              See more
            </Link>
          </Container>
        </article> 
      </section>
    )
  }
}

export default WPHome;