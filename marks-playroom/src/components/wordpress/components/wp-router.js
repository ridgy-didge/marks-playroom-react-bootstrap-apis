import React, { Component } from 'react';
import { CSSTransition, TransitionGroup } from 'react-transition-group';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'

//bootstrap

// styles
// import '../App.scss';

// import Menuitems from './data/main-nav-data'

import WPHome from './wp-home';
import SingleProductDetails from './products/single-product-details';
import WPProducts from './products/all-products';
import WPPosts from './posts/wp-posts';
import WPPost from './posts/wp-post';
import WPMenu from './wp-menu';
import Error404 from '../../error404/error-404';

class WordressRouter extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }
  
  render() {
    return (
      <Router >
        <WPMenu/>
        
          <Route
            render={({ location }) => {
              const { pathname } = location;
              return (
                <TransitionGroup className="wp-transition-group">
                  <CSSTransition 
                    key={pathname}
                    classNames={'item'}
                    timeout={{
                      enter:400,
                      exit:400
                    }}
                  >
                    <section className="route-section wp-page">
                      <Route
                        location={location}
                        render={() => (
                          <Switch>
                            <Route exact path="/wordpress" component={WPHome} />
                            <Route exact path="/wordpress/products" component={WPProducts} />
                            <Route exact path="/wordpress/product/:slug" component={SingleProductDetails} />
                            <Route exact path="/wordpress/posts" component={WPPosts} />
                            <Route exact path="/wordpress/posts/:slug" component={WPPost} />
                            <Route component={Error404} /> {/* keep error404 last */}
                          </Switch>
                        )}
                      />
                    </section>
                  </CSSTransition>
                </TransitionGroup>
              );
            }}
          />
        
      </Router>
    )
  }
}

export default WordressRouter;