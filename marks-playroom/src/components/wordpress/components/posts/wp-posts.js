import React, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';

import '../styles/partials/wp-posts.scss';

class WPPosts extends Component {
  constructor(props) {
    super(props);
    this.state = {
      posts: [],
    };
  }
  
  componentDidMount() {
    const pages = "posts";   // filter post result to post type
    const baseUrl = "https://dev-ridgy-didge.co.uk/wp-rest-api-tests/wp-json/wp/v2/";

    axios
    .get(
      baseUrl + pages
    )
    .then(res => { // fetch posts
      this.setState({ 
        posts: res.data      
      })
    })
    .catch(error => console.log(error))
  }

  render() {
    console.log(this.state.posts)
    
    let listposts = this.state.posts.map((post, index) => {
      return(
        <div key={index} className="postlist-item">
          <h4>{post.title.rendered}</h4>
          <div className="postlist-content">
            <p dangerouslySetInnerHTML={{__html: post.excerpt.rendered}}/> 
            {/* dangerouslySetInnerHTML eliminates the html tags */}
            <Link to={{
              pathname: `/wordpress/posts/${post.slug}`,
              state: {
                postSlug: post.slug,
                postID: post.id,
                postTitle: post.title.rendered,
                postMedia: post.featured_media,
                postImgUrl: post.image_url,
                postDesc: post.content.rendered,
                acfDetails: post.acfDetails,
              }
            }}>
              {post.title.rendered}
            </Link>
          </div>
        </div>
      )
    })

    return (
      <article>
        <h1>Posts</h1>
          <div className="postlist-container">
            {listposts}
          </div>
      </article>
    );
  }
}

export default WPPosts;
