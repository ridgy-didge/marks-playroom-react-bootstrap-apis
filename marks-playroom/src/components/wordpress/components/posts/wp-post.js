import React, { Component } from 'react';

import '../styles/partials/wp-post.scss';

class WPPost extends Component {
  constructor(props) {
    super(props);
    this.state = {
      postTitle: this.props.location.state.postTitle,
      postID: this.props.location.state.postID, 
      postSlug: this.props.location.state.postSlug,
      postDesc: this.props.location.state.postDesc,
    };
  }

  render() {
    let ID =  this.state.postID;
    let Title =  this.state.postTitle;
    let Desc =  this.state.postDesc;
    console.log(this.state)

    return (
      <div className="wp-post">
        <div className="wp-post-container container">
          <div className="wp-post-header">
            <h2 className="wp-post-title">Wordpress API - Post </h2>
          </div>   
          <div className="wp-post-container ">
            <div className="wp-post-main">
              <div className="wp-post-header">
                <h2>{Title}</h2>
                <div className="wp-post-header-details">
                  <p>Post ID: {ID}</p>
                </div>
              </div>
            </div>
            <div className="wp-post-wrapper">
              <p dangerouslySetInnerHTML={{__html: Desc}}></p>
            </div>     
          </div>
        </div>
      </div>
    );
  }
}

export default WPPost;
