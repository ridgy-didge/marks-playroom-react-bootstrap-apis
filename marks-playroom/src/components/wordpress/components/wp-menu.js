import React, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';

//bootstrap imports

//styles
import './styles/wp-menu.scss';

// animated SVGs

// animated svg buttons

//images

class WPMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: "",
      content: "",
      featured_media: "",
     }
  }

  componentDidMount() {
    const baseUrl = "https://dev-ridgy-didge.co.uk/wp-rest-api-tests/wp-json/wp/v2/pages/272";

    axios
    .get(
      baseUrl 
    )
    .then(res => { // fetch products page and set states
      this.setState({ 
        title: res.data.title.rendered,
        content: res.data.content.rendered,
        featured_media: res.data.featured_media,
      })  
    })
    .catch(error => console.log(error))
  }

  stripHtml(html){  // strip html tags from excerpt or anything else`
    // Create a new div element
    var temporalDivElement = document.createElement("div");
    // Set the HTML content with the providen
    temporalDivElement.innerHTML = html;
    // Retrieve the text property of the element (cross-browser support)
    return temporalDivElement.textContent || temporalDivElement.innerText || "";
  }

  render() {

    return (
      <section id="wordpressPage" className="wordpress-menu">
        <div className="wordpress-menu-item">
          <Link className="btn-page btn" to="/wordpress">
            WP
          </Link>
        </div>
        <div className="wordpress-menu-item">
          <Link className="btn-page btn" to="/wordpress/products">
            WP Products
          </Link>
        </div>
      </section>
    )
  }
}

export default WPMenu;