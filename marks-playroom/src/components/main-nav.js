import React, { Component } from 'react';
import { Link } from 'react-router-dom';
//bootstrap
import { Nav, Row, Navbar, Button, Container } from 'react-bootstrap';
//styles
import '../styles/_base.scss';
//images
import NavToggler from './svgs/nav-toggle01/toggle-nav-01' // relative path to image 

class MainNav extends Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
          open: false,
        };
    }

    render() {
        const { open } = this.state;

        const Toggler = 
            <Button
                className="navbar-toggler"
                onClick={() => this.setState({ open: !open })}
                aria-controls="nav-collapse"
                aria-expanded={open}
                >
                <NavToggler/>
            </Button>;

        const NavBar = 
            <Nav id="nav-collapse">
                {Toggler}
                <Navbar.Brand><Link className="nav-link" to="/">Marks Playroom</Link></Navbar.Brand>
                <Nav.Item><Link className="nav-link" to="/">Home</Link></Nav.Item>
                <Nav.Item><Link className="nav-link" to="/f1" role="button" aria-haspopup="true" aria-expanded="false">Formula One</Link></Nav.Item>
                <Nav.Item><Link className="nav-link" to="/starwars" role="button" aria-haspopup="true" aria-expanded="false">Star Wars</Link></Nav.Item>
                <Nav.Item><Link className="nav-link" to="/blender" role="button" aria-haspopup="true" aria-expanded="false">Blender</Link></Nav.Item>
                <Nav.Item><Link className="nav-link" to="/wordpress" role="button" aria-haspopup="true" aria-expanded="false">Wordpress</Link></Nav.Item>
            </Nav>;

        return (
            <Row className="nav-container">
                <Container>
                    <Navbar className="mobile-nav" expand="lg">
                        <Navbar.Brand><Link className="nav-link" to="/">Marks Playroom</Link></Navbar.Brand>
                        {Toggler}
                        <Navbar.Collapse id="nav-collapse" in={open}>
                            {NavBar}
                        </Navbar.Collapse>
                    </Navbar>
                    <Navbar className="desktop-nav" >
                        {NavBar}
                    </Navbar>
                </Container>
            </Row>
        );
    }
}

export default MainNav;
