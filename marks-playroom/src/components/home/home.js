import React, { Component } from 'react';
import { Link } from 'react-router-dom';
//bootstrap
import { Container, Button, Col } from 'react-bootstrap';

//styles
import './home.scss';

// animated SVGs
import StarwarsLogo from './components/svgs/starwars-logo/starwars-logo';
import StarwarsBG from './components/svgs/starwars-bg/starwars-bg';
import FormulaOneLogo from './components/svgs/f1-logo/f1-logo';
import FormulaOneBG from './components/svgs/f1-logo-bg/f1-logo-bg';
import BlenderLogo from './components/svgs/blender-logo/blender-logo';
import BlenderBG from './components/svgs/blender-logo-bg/blender-logo-bg';
import WordpressLogo from './components/svgs/wordpress-logo/wordpress-logo';
import WordpressBG from './components/svgs/wordpress-logo-bg/wordpress-logo-bg';


// animated svg buttons
import ArrowButton from './components/svgs/arrow-button/arrow-button';

const Menuitems = [
  { id: 1, path: '/starwars', slug: 'star-wars', title: 'Star Wars', text: 'Data from Star Wars Api (SWAPI)', },
  { id: 2, path: '/f1', slug: 'f1', title: 'Formula One', text: 'Data from Formula One Api (Ergast)' },
  { id: 3, path: '/blender', slug: 'blender', title: 'Blender', text: 'Blender images i have created' },
  { id: 4, path: '/wordpress', slug: 'wordpress', title: 'Wordpress', text: 'Wordpress Api Data' },
];

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      id: '',
      active: false,
    };
    this.handleClick = this.handleClick.bind(this);
    this.baseState = this.state 
  }

  image(prop) {
    const slug = prop;
    if (slug === 'star-wars') {
      return <StarwarsLogo/> 
    } else if (slug === 'f1') {
      return <FormulaOneLogo />;
    } else if (slug === 'blender')  {
      return <BlenderLogo />;
    } else if (slug === 'wordpress'){
      return <WordpressLogo />;
    }
  } 

  imageBG(prop) {
    const slug = prop;
    if (slug === 'star-wars') {
      return <StarwarsBG/> 
    } else if (slug === 'f1') {
      return <FormulaOneBG />;
    } else if (slug === 'blender')  {
      return <BlenderBG />;
    } else if (slug === 'wordpress'){
      return <WordpressBG />;
    }
  } 


  handleClick(item) {
    this.setState(this.baseState) // reset state to remove last selected item
    this.setState(state => ({
      open: !state.open,
      id: item, // set to match state id to className="item-xx"
      active: !state.open,
    }));

  }

  render() {
    console.log(this.state);
    return (
      <div className="home">
        <Container> 
          <section>
            <h1 className="page-title" >Home</h1>
              <Col className="home-content">
                {Menuitems.map((item) => 
                  <div key={item.id} className="home-item">
                    <div className="home-item-image">
                      <div className={"item-" + item.id + " home-item-image-top"} open={ this.state.open && this.state.id === "item-" + item.id  } >
                        {this.image(item.slug)}
                      </div>
                      {this.imageBG(item.slug)}
                      <p className="home-item-title">{item.text}</p>
                      <Link 
                        to={item.path} 
                        className={item.slug + "-button btn-arrow btn-link"} 
                        alt={item.title + " link"}
                      >
                        <ArrowButton />
                      </Link>
                     
                    </div>
                    <div className="home-item-text">
                      <p className="home-item-title">{item.title}</p>
                      <Button 
                        onClick={() => this.handleClick("item-" + item.id)}
                        className={item.slug + "-button btn-arrow"}
                        active={this.state.active && this.state.id === "item-" + item.id}
                        alt={item.title + " link"}
                      >
                        <ArrowButton />
                      </Button>
                    </div>
                  </div>
                )}
              </Col>
          </section>   
        </Container>
      </div>
    )
  }
}

export default Home;