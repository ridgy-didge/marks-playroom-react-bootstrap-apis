import React, { Component } from 'react';

//styles
import './style/base.scss';


class FormulaOneLogo extends Component {
  render() {
    return (
      <svg id="f1LogoTop" className="logo" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1000 500" enableBackground="new 0 0 540 122.523">
        <title>formula one logo</title>
        <g id="f1-logo-container">
          <polygon id="one"  className="cls-red" points="837.46 131.96 593.31 387 744.95 387 989.1 131.96 837.46 131.96"/>
          <path id="f1-top" className="cls-red" d="M13.44,387l182.7-190.84C237,153.48,286.73,132,344.51,132h472L724.92,227.6H368.49c-42,0-78.19,15.65-107.9,46.69L152.69,387Z"/>
          <path id="f1-bottom" className="cls-red" d="M176.53,387,280.35,278.56c22.28-23.28,49.41-35,80.92-35H709.66l-84.88,88.67H386c-19.44,0-36.16,7.23-49.91,21.59L304.33,387Z"/>
          <polygon id="t" className="cls-red" points="825.27 390.18 825.27 362.71 815.59 362.71 815.59 357.13 841.54 357.13 841.54 362.71 831.9 362.71 831.9 390.18 825.27 390.18"/>
          <polygon id="m" className="cls-red" points="845.73 390.18 845.73 357.13 855.65 357.13 861.52 379.67 867.31 357.13 877.25 357.13 877.25 390.18 871.13 390.18 871.11 364.16 864.66 390.18 858.3 390.18 851.88 364.16 851.85 390.18 845.73 390.18"/>
        </g>
      </svg>
    )
  }
}

export default FormulaOneLogo;