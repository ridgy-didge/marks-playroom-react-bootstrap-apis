import React, { Component } from 'react';

//styles
import './style/base.scss';

class ArrowButton extends Component {
  render() {
    return (
			<svg id="arrow-button" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100">
				<title>arrow button</title>
				<g id="button">
					<circle className="cls-1" cx="50" cy="50" r="49"/>
					<polyline className="cls-2" points="57.35 31.63 37.75 51.23 56.13 69.6"/>
				</g>
			</svg>
    )
  }
}

export default ArrowButton;